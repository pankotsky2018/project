const {Router} = require("express")
const PriceList = require("../models/priceList")
const router = Router()
var LocalStorage = require('node-localstorage').LocalStorage,
    localStorage = new LocalStorage('./scratch');

router.get('/', async (req, res) =>{
    const pricesGet = await PriceList.find({}).lean()
    const curuser = JSON.parse(localStorage.getItem('user'))
    console.log(curuser)

    res.render('index', {
        title: 'Price List',
        isIndex: true,
        pricesGet,
        curuser
    })
})

router.get('/create', (req, res) => {
    const curuser = JSON.parse(localStorage.getItem('user'))

    res.render('create', {
        title: 'Create',
        isCreate: true,
        curuser
    })
})

router.get('/update/:id', async (req, res) => {
    const id = req.params.id
    const pricesUpdate = await PriceList.findById(id).lean()
    const curuser = JSON.parse(localStorage.getItem('user'))

    res.render('update', {
        title: 'Update',
        isUpdate: true,
        pricesUpdate,
        curuser
    })
})


router.route('/update').post(async (req,res) =>{
    const id = req.body.idUpdate
    await PriceList.findByIdAndUpdate(id,{
        name: req.body.nameOfUpdate,
        price: req.body.priceOfUpdate
    })
    res.redirect('/')
})

router.route('/delete').post(async (req,res) =>{
    const id = req.body.idUpdate
    await PriceList.findByIdAndDelete(id)
    res.redirect('/')
})

router.post('/create', async (req, res) =>{
    const price = new PriceList({
        name: req.body.nameOf,
        price: req.body.priceOf
    })

    await price.save()
    res.redirect('/')
})

module.exports = router