const {Router} = require("express")
const users = require("../models/user")
const router = Router()
var LocalStorage = require('node-localstorage').LocalStorage,
    localStorage = new LocalStorage('./scratch');

router.get("/users", async (req, res) =>{
    const curuser = JSON.parse(localStorage.getItem('user'))
    const userList = await users.find({}).lean()

    res.render('users', {
        title: 'Users',
        isUsers: true,
        curuser,
        userList
    })
})

router.post("/searchusers", async (req, res) =>{
    const curuser = JSON.parse(localStorage.getItem('user'))
    const find = req.body.search;
    const userList = await users.find({name: find}).lean()

    res.render('users', {
        title: 'Users',
        isUsers: true,
        curuser,
        userList
    })
})

router.get("/register", async (req, res) => {
    const curuser = JSON.parse(localStorage.getItem('user'))

    res.render('register', {
        title: 'Register',
        isReg: true,
        curuser
    })
})

router.post('/register', async (req, res) => {
    const user = new users({
        name: req.body.name,
        surname: req.body.surname,
        mobile: req.body.mobile,
        acces: req.body.accessLevel
    })

    await user.save()
    res.redirect('/')
})

router.get("/login", async (req, res) => {
    res.render('login', {
        title: 'login',
        isLogin: true,
    })
})

router.post('/login', async (req, res) => {
    const {mobile} = req.body;

    const user = await users.findOne({mobile})
    localStorage.setItem("user", JSON.stringify(user))
    res.redirect('/')
})

router.get('/logout', async (req,res) =>{
    localStorage.clear()
    res.redirect('/')
})

module.exports = router