const {Router} = require("express")
const metalGiven = require("../models/metalgive")
const partnersList = require("../models/partners")
const router = Router()
var LocalStorage = require('node-localstorage').LocalStorage,
    localStorage = new LocalStorage('./scratch');

router.get("/givenmetalview", async (req, res) => {
    const curuser = JSON.parse(localStorage.getItem('user'))
    const metal = await metalGiven.find({}).lean()

    res.render('givenmetalview', {
        title: 'All we ever given',
        isMG: true,
        curuser,
        metal
    })
})

router.post("/sortgives", async (req, res) => {
    const curuser = JSON.parse(localStorage.getItem('user'))
    const metal = await metalGiven.find({}).sort({date: 'desc'}).lean()
    res.render('givenmetalview', {
        title: 'All we ever given',
        isMG: true,
        curuser,
        metal
    })
})

router.post("/searchgives", async (req, res) => {
    const curuser = JSON.parse(localStorage.getItem('user'))
    const find = req.body.search;
    const metal = await metalGiven.find({name: find}).lean()
    const metalList = await metalGiven.find({company_name: find}).lean()

    res.render('givenmetalview', {
        title: 'All we ever given',
        isMG: true,
        curuser,
        metal,
        metalList
    })
})

router.get("/metalgive", async (req, res) => {
    const curuser = JSON.parse(localStorage.getItem('user'))

    res.render('metalgive', {
        title: 'Send to our partners',
        isMS: true,
        curuser
    })
})

router.post("/metalgive", async (req, res) => {
    const name = req.body.compName;
    const compPhone = await partnersList.findOne({company_name: name})

    const newsending = new metalGiven({
        name: req.body.name,
        weight: req.body.weight,
        company_name: name,
        company_mobile: compPhone.company_mobile,
        money: req.body.money,
        date: req.body.date
    })

    await newsending.save()
    res.redirect('/givenmetalview')
})

router.get('/updatesending/:id', async (req, res) => {
    const id = req.params.id
    const metal = await metalGiven.findById(id).lean()
    const curuser = JSON.parse(localStorage.getItem('user'))

    res.render('updatesending', {
        title: 'Update',
        isMGU: true,
        metal,
        curuser
    })
})

router.route('/updatesendings').post(async (req,res) =>{
    const id = req.body.idUpdate
    const name = req.body.compName;
    const compPhone = await partnersList.findOne({company_name: name})

    await metalGiven.findByIdAndUpdate(id,{
        name: req.body.name,
        weight: req.body.weight,
        company_name: name,
        company_mobile: compPhone.company_mobile,
        money: req.body.money,
        date: req.body.date
    })
    res.redirect('/givenmetalview')
})

router.route('/sendingdelete').post(async (req,res) =>{
    const id = req.body.idUpdate
    await metalGiven.findByIdAndDelete(id)
    res.redirect('/givenmetalview')
})

module.exports = router