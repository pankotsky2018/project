const {Router} = require("express")
const metal = require("../models/metaladd")
const PriceList = require("../models/priceList")
const user = require("../models/user")
const router = Router()
var LocalStorage = require('node-localstorage').LocalStorage,
    localStorage = new LocalStorage('./scratch');

router.get("/metalview", async (req, res) => {
    const curuser = JSON.parse(localStorage.getItem('user'))
    const metals = await metal.find({}).lean()

    res.render('metalview', {
        title: 'All we ever got',
        isMV: true,
        curuser,
        metals
    })
})

router.post("/searchmetals", async (req, res) => {
    const curuser = JSON.parse(localStorage.getItem('user'))
    const find = req.body.search;
    const metals = await metal.find({man_name: find}).lean()
    const metalList = await metal.find({name: find}).lean()
    res.render('metalview', {
        title: 'All we ever got',
        isMV: true,
        curuser,
        metals,
        metalList
    })
})

router.post("/sortmetals", async (req, res) => {
    const curuser = JSON.parse(localStorage.getItem('user'))
    const metalList = await metal.find({}).sort({date: 'desc'}).lean()
    res.render('metalview', {
        title: 'All we ever got',
        isMV: true,
        curuser,
        metalList
    })
})

router.get("/metaladd", async (req, res) => {
    const curuser = JSON.parse(localStorage.getItem('user'))

    res.render('metaladd', {
        title: 'Add new metal',
        isMA: true,
        curuser
    })
})

router.post("/metaladd", async (req, res) => {
    const name = req.body.name
    const weight = req.body.weight
    const matName = await PriceList.findOne({name})
    const money = weight * matName.price

    const newmetal = new metal({
        name: name,
        weight: weight,
        man_name: req.body.user,
        man_mobile: req.body.mobile,
        money: money,
        date: req.body.date
    })

    await newmetal.save()
    res.redirect('/metalview')
})

router.get('/updatemetal/:id', async (req, res) => {
    const id = req.params.id
    const metals = await metal.findById(id).lean()
    const curuser = JSON.parse(localStorage.getItem('user'))

    res.render('metalupdate', {
        title: 'Update',
        isMU: true,
        metals,
        curuser
    })
})

router.route('/updatemetal').post(async (req, res) => {
    const id = req.body.idUpdate
    const name = req.body.name
    const weight = req.body.weight
    const matName = await PriceList.findOne({name})
    const price = weight * matName

    await metal.findByIdAndUpdate(id, {
        name: name,
        weight: weight,
        man_name: req.body.user,
        man_mobile: req.body.mobile,
        money: price,
        date: req.body.date
    })
    res.redirect('/metalview')
})

router.route('/deletemetal').post(async (req, res) => {
    const id = req.body.idUpdate
    await metal.findByIdAndDelete(id)
    res.redirect('/metalview')
})



module.exports = router