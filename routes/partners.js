const {Router} = require("express")
const partners = require("../models/partners")
const router = Router()
var LocalStorage = require('node-localstorage').LocalStorage,
    localStorage = new LocalStorage('./scratch');

router.get("/partnersview", async (req, res) => {
    const curuser = JSON.parse(localStorage.getItem('user'))
    const partnerList = await partners.find({}).lean()

    res.render('partnersview', {
        title: 'Our partners',
        isPV: true,
        curuser,
        partnerList
    })
})

router.post("/searchpartners", async (req, res) => {
    const curuser = JSON.parse(localStorage.getItem('user'))
    const find = req.body.search;
    const partnerList = await partners.find({company_name: find}).lean()
    const partner = await partners.find({company_tag: find}).lean()
    res.render('partnersview', {
        title: 'Our partners',
        isPV: true,
        curuser,
        partnerList,
        partner
    })
})

router.get('/addpartner', (req, res) => {
    const curuser = JSON.parse(localStorage.getItem('user'))

    res.render('partnercreate', {
        title: 'Create',
        isPC: true,
        curuser
    })
})

router.post('/addpartner', async (req, res) => {
    const partner = new partners({
        company_name: req.body.name,
        company_tag: req.body.tag,
        company_mobile: req.body.mobile,
        company_email: req.body.email,
        company_address: req.body.address
    })

    await partner.save()
    res.redirect('/partnersview')
})

router.get('/updatepartners/:id', async (req, res) => {
    const id = req.params.id
    const partner = await partners.findById(id).lean()
    const curuser = JSON.parse(localStorage.getItem('user'))

    res.render('updatepartners', {
        title: 'Update',
        isPU: true,
        partner,
        curuser
    })
})

router.route('/updatepartners').post(async (req, res) => {
    const id = req.body.idUpdate

    await partners.findByIdAndUpdate(id, {
        company_name: req.body.name,
        company_tag: req.body.tag,
        company_mobile: req.body.mobile,
        company_email: req.body.email,
        company_address: req.body.address
    })
    res.redirect('/partnersview')
})

router.route('/partnerdelete').post(async (req, res) => {
    const id = req.body.idUpdate
    await partners.findByIdAndDelete(id)
    res.redirect('/partnersview')
})

module.exports = router