const Chart = require("chart.js")
const {Router} = require("express")
const metal = require("../models/metaladd")
const PriceList = require("../models/priceList")
const user = require("../models/user")
const partnersList = require("../models/partners")
const metalGiven = require("../models/metalgive")
const router = Router()
var LocalStorage = require('node-localstorage').LocalStorage,
    localStorage = new LocalStorage('./scratch');

router.get("/stat", async (req, res) =>{
    const curuser = JSON.parse(localStorage.getItem('user'))
    const metals = await metal.find({})
    const gives = await metalGiven.find({})
    const dict1 = new Map()
    const dict2 = new Map()
    metals.forEach(m => {
        if(dict1.get(m.name) !== undefined){
            dict1.set(m.name, parseFloat(dict1.get(m.name)) + parseFloat(m.weight))
        } else {
            dict1.set(m.name, parseFloat(m.weight))
        }
    })
    gives.forEach(m => {
        if(dict2.get(m.name) !== undefined){
            dict2.set(m.name, parseFloat(dict2.get(m.name)) + parseFloat(m.weight))
        } else {
            dict2.set(m.name, parseFloat(m.weight))
        }
    })
    res.render('stat', {
        title: 'Statistics',
        isStat: true,
        curuser,
        dict1,
        dict2
    })
})
module.exports = router

