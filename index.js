const express = require("express")
const mongoose = require("mongoose")
const exphbs = require("express-handlebars")
const priceListRoutes = require('./routes/priceList')
const registerRouters = require('./routes/user')
const metalRouters = require('./routes/metal')
const statRouters = require('./routes/stat')
const partnersRouters = require('./routes/partners')
const metalGiveRouters = require('./routes/metalgive')
const PORT = process.env.PORT || 3000
const app = express()

const hbs = exphbs.create({
    defaultLayout: 'main',
    extname: 'hbs'
})

app.engine('hbs', hbs.engine)
app.set('view engine', 'hbs')
app.set('views', 'views')

app.use(express.urlencoded({extended: true}))

app.use(priceListRoutes)
app.use(registerRouters)
app.use(metalRouters)
app.use(partnersRouters)
app.use(metalGiveRouters)
app.use(statRouters)

//mongodb+srv://UmerT:123321@metal.dky9z.mongodb.net/price
//mongodb://127.0.0.1:27017/Metal

async function start() {
    try {
        await mongoose.connect('mongodb://127.0.0.1:27017/Metal', {
            useNewUrlParser: true,
            useFindAndModify: false,
            useUnifiedTopology: true
        })
        app.listen(PORT, () => {
            console.log("Server has been started...")
        })
    } catch (e) {
        console.log(e)
    }
}


start()
