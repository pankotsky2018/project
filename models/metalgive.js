const {Schema, model} = require('mongoose')

const schema = new Schema({
    name: {
        type: String,
        required: true
    },
    weight: {
        type: String,
        required: true
    },
    company_name: {
        type: String,
        required: true
    },
    company_mobile: {
        type: String,
        required: true
    },
    money: {
        type: String,
        required: true
    },
    date: {
        type: String,
        required: true
    }
})

module.exports = model('metal_given', schema)