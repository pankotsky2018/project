const {Schema, model} = require('mongoose')

const schema = new Schema({
    company_name: {
        type: String,
        required: true
    },
    company_tag: {
        type: String,
        required: true
    },
    company_mobile: {
        type: String,
        required: true,
        unique: true
    },
    company_email: {
        type: String,
        required: true
    },
    company_address: {
        type: String,
        required: true
    }
})

module.exports = model('partners', schema)